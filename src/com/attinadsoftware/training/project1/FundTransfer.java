package com.attinadsoftware.training.project1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class FundTransfer implements DataFetch,Transaction,Insertion,Updation{
	
	int cusId,cusIdTo,amount,cus,curBal,accBal;
	long accNum,accNumTo;
	String name,myname,type = "Fund Transfer",updatequeryone,selectonequery,selectqueryone,insertquery,mode,selectquery;
	@Override
	public void getData() {
		Scanner scn=new Scanner(System.in);;
		System.out.println("Enter your customer id");
		cusId=scn.nextInt();
		System.out.println("Enter your account number");
		accNum = scn.nextLong();
		System.out.println("Enter the amount to be transfered");
		amount=scn.nextInt();
		System.out.println("Enter the customer id of the recepient");
		cusIdTo=scn.nextInt();
		
		    doWithdraw();
			doDeposit();
			
			
		
	}

	@Override
	public void doWithdraw() {
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			selectquery="select account.acc_bal,transaction.acc_num from account,transaction where account.cus_id=? and transaction.cus_id=?";
			PreparedStatement stmt = con.prepareStatement(selectquery);
			stmt.setInt(1,cusId);
			stmt.setInt(2,cusId);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				accBal=rs.getInt(1);
				accNum=rs.getLong(2);
			}
			if((accBal-amount)>1000) {
				insert();
				update();
					
				}else {
					System.out.println("Transaction failed. your account  balance is less than 1000");
				}
			}catch(Exception e) {
				System.out.println(e);
			}
		
	}
	@Override
	public void doDeposit() {
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			selectonequery="select name,acc_num from customer,transaction where customer.cus_id=? and transaction.cus_id=?";
			PreparedStatement stmt = con.prepareStatement(selectonequery);
			stmt.setInt(1,cusIdTo);
			stmt.setInt(2,cusIdTo);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				name=rs.getString(1);
				accNumTo=rs.getLong(2);
			}
			selectquery="select name from customer, where cus_id=?";
			PreparedStatement pstmt = con.prepareStatement(selectquery);
			stmt.setInt(1,cusId);
			java.sql.ResultSet r = stmt.executeQuery();
			while(r.next()) {
				myname=r.getString(1);
			}
			}catch(Exception e) {
				System.out.println(e);
			}
		
		insert(myname);
		update(myname);
	}

	

	@Override
	public void insert(String myname) {
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			insertquery="insert into transaction(acc_num,cus_id,type,amount,mode) value(?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(insertquery);
			
			stmt.setLong(1,accNumTo);
			stmt.setInt(2,cusIdTo);
			stmt.setString(3,type);
			stmt.setInt(4,amount);
			stmt.setString(5,myname);
			stmt.executeUpdate();
		}catch(Exception e) {
			System.out.println(e);
		}
		
	}

	
	@Override
	public void update() {
		try {
		SqlConnection c = new SqlConnection();
		Connection con = c.getConnection();
		selectqueryone="select acc_bal from account where cus_id=?";
		PreparedStatement stmt = con.prepareStatement(selectqueryone);
		stmt.setInt(1,cusId);
		java.sql.ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			accBal=rs.getInt(1);
		}
			curBal=accBal-amount;
		updatequeryone="update account set acc_bal=? where cus_id=?";
		PreparedStatement prestmt = con.prepareStatement(updatequeryone);
		prestmt.setInt(1,curBal);
		prestmt.setInt(2,cusId);
		prestmt.executeUpdate();
		//System.out.println("Transaction success");
		}catch(Exception e) {System.out.println(e);}
	
		
	}

	@Override
	public void update(String myname) {
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			selectqueryone="select acc_bal from account where cus_id=?";
			PreparedStatement stmt = con.prepareStatement(selectqueryone);
			stmt.setInt(1,cusIdTo);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				accBal=rs.getInt(1);
			}
				curBal=accBal+amount;
			updatequeryone="update account set acc_bal=? where cus_id=?";
			PreparedStatement prestmt = con.prepareStatement(updatequeryone);
			prestmt.setInt(1,curBal);
			prestmt.setInt(2,cusIdTo);
			prestmt.executeUpdate();
			System.out.println("Transaction success");
			}catch(Exception e) {System.out.println(e);}
		
	}

	
	@Override
	public void insert() {
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			insertquery="insert into transaction(acc_num,cus_id,type,amount,mode) value(?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(insertquery);
			
			stmt.setLong(1,accNum);
			stmt.setInt(2,cusId);
			stmt.setString(3,type);
			stmt.setInt(4,amount);
			stmt.setString(5,name);
			stmt.executeUpdate();
		}catch(Exception e) {
			System.out.println(e);
		}
	}

	
}
