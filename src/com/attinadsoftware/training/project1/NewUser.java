package com.attinadsoftware.training.project1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class NewUser implements DataFetch, Insertion,Selection{
	String name, fname, email, PAN;
	int cusId;

	public void openAccount() {
		getData();
		insert();
		select();
	}

	public void getData() {
		Scanner scn = new Scanner(System.in);
		System.out.println("Enter your name");
		name = scn.next();
		System.out.println("Enter your Father's name");
		fname = scn.next();
		System.out.println("Enter your Email id");
		email = scn.next();
		System.out.println("Enter your pan number");
		PAN = scn.next();
		while(PAN.length()!=10) {System.out.println("Invalid pan number");}
		scn.close();
	}

	@Override
	public void insert() {
		if (!name.equals(" ") && !fname.equals(" ") && !email.equals(" ") && !PAN.equals(" ")) {
			try {
				SqlConnection c = new SqlConnection();
				Connection con = c.getConnection();

				String insertquery = "insert into customer(Name,FathersName,Email,PAN) values(?,?,?,?)";
				PreparedStatement prestmt = con.prepareStatement(insertquery);
				prestmt.setString(1, name);
				prestmt.setString(2, fname);
				prestmt.setString(3, email);
				prestmt.setString(4, PAN);
				prestmt.executeUpdate();
				
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		} else {
			System.out.println("Inavlid Details ");
		}

	}

	@Override
	public void select() {
		try {
			String PAN="FDWS123456";
			System.out.print("Your customer id is:");
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			String selectquery = "select cus_id from customer where customer.PAN=?";
			PreparedStatement stmt = con.prepareStatement(selectquery);
			stmt.setString(1,PAN);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				cusId= rs.getInt(1);
				System.out.print(rs.getInt(1));
				NewAccount newAccount = new NewAccount(cusId);
			}
			//NewAccount newAccount = new NewAccount(cusId);
			
			rs.close();
			con.close();
		}catch(Exception e) {
			System.out.println(e);
		}
		
	}

	@Override
	public void insert(String name) {
		// TODO Auto-generated method stub
		
	}

}
