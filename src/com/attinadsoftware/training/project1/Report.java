package com.attinadsoftware.training.project1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class Report {
	int count = 0,totalamount=0,condone=0,condtwo=0,totalcount=0;
	long accNum;
	String selectqueryone,selectquerytwo,selectquerythree,selectqueryfour,type="Deposit";
	PreparedStatement stmt;
	java.sql.ResultSet rs;
	Scanner scn = new Scanner(System.in);
	public void accountReport() {
		System.out.println("\n1.Report based on duration \n 2.Suspicious account");
		int option=scn.nextInt();
		if(option==1) {
			durationReport();
		}else {
			suspAccount();
		}
	}
	
	public void durationReport() {
		
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			stmt = con.prepareStatement(" select count(t_id) from transaction where DateofTransaction between '2017-10-22' and '2017-10-25'; ");
			rs = stmt.executeQuery();
			while(rs.next()) {
			totalcount=rs.getInt(1);
			System.out.println("The number of transaction between 22 oct to 25th oct is "+ totalcount);
			}
			
		}catch(Exception e) {
			System.out.println(e);
		}
	}
	public void suspAccount() {
		try {
		SqlConnection c = new SqlConnection();
		Connection con = c.getConnection();
		selectqueryone="select count(acc_num),acc_num from transaction where type=? group by acc_num";
        stmt = con.prepareStatement(selectqueryone);
		stmt.setString(1,type);
	    rs = stmt.executeQuery();
	    //System.out.println("first susp");
		while(rs.next()) {
			count=rs.getInt(1);
			accNum=rs.getInt(2);
			if(count>10) {
				System.out.println("The account is suspicious whose account number is"+accNum);
			}
		}
		selectquerytwo="select sum(amount),acc_num from transaction where type=? group by acc_num";
		stmt = con.prepareStatement(selectquerytwo);
		stmt.setString(1,type);
		rs = stmt.executeQuery();
		// System.out.println("2 susp");
		while(rs.next()) {
			totalamount=rs.getInt(1);
			accNum=rs.getInt(2);
			if(totalamount>100000) {
				System.out.println("The account is suspicious whose account number is"+accNum);
			}
		}
		selectquerythree="select count(acc_num) from transaction where type='Deposit' and mode='cash' group by acc_num";
		selectqueryfour="select count(acc_num),acc_num from transaction where type='Deposit' group by acc_num";
		stmt = con.prepareStatement(selectquerythree);
		rs = stmt.executeQuery();
		 //System.out.println("3 susp");
		while(rs.next()) {
			condone=rs.getInt(1);
			stmt = con.prepareStatement(selectqueryfour);
			rs = stmt.executeQuery();
			while(rs.next()) {
				condtwo=rs.getInt(1);
				accNum=rs.getLong(2);
			if(condone>(condtwo/2)) {
				System.out.println("The account is suspicious "+ accNum);
			}}
		}
		rs.close();	
		}catch(Exception e) {
		
			
		}
	}

	
		
		
	

}
