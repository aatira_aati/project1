package com.attinadsoftware.training.project1;

public interface Transaction {
     public void doDeposit();
     public void doWithdraw();
}
