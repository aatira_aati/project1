package com.attinadsoftware.training.project1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class TransactionProcess implements DataFetch,Transaction,Insertion,Updation{
    int tran,cusId,mode,accBal,amount,curBal=0;
    long accNum;
    String selectquery,selecttwoquery,updateonequery,selectonequery,insertquery,modeType;
	
    @Override
	public void getData() {
		Scanner scn = new Scanner(System.in);
		System.out.println("What transaction you wish to do?\n 1.Deposit \n 2.Withdrawal \n 3.Fund Transfer");
		tran = scn.nextInt();
		doTransfer();
		scn.close();
	}
	
	
    public void doTransfer() {
		if(tran==1) {
			doDeposit();	
		}else if(tran==2) {
			doWithdraw();
		}else {	
			FundTransfer fundTransfer=new FundTransfer();
			fundTransfer.getData();
		}
	}
	
	
    
    
    @Override
	public void doDeposit() {
		String type="Deposit";
		Scanner scn = new Scanner(System.in);
		System.out.println("Enter the customer id");
		cusId=scn.nextInt();
		System.out.println("Enter the amount to be deposited");
		amount=scn.nextInt();
		System.out.println("Enter the mode of transaction \n 1. cash\n 2.cheque");
		mode=scn.nextInt();
		if(mode==1) {
			modeType="cash";
		}else {
			modeType="cheque";
		}
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			selectonequery="select transaction.acc_num from transaction where cus_id=?";
			PreparedStatement stmt = con.prepareStatement(selectonequery);
			stmt.setInt(1,cusId);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				accNum=rs.getInt(1);
				
			}
			insert(type);
			update(type);
			
		}catch(Exception e) {
			System.out.println(e);
		}
		
	}

    
    
    
    @Override
	public void doWithdraw() {
		String type ="Withdrawal";
		Scanner scn = new Scanner(System.in);
		System.out.println("Enter the customer id");
		cusId=scn.nextInt();
		System.out.println("Enter the amount to be withdraw");
		amount=scn.nextInt();
		System.out.println("Enter the mode of transaction \n 1. cash\n 2.cheque");
		mode=scn.nextInt();
		if(mode==1) {
			modeType="cash";
		}else {
			modeType="cheque";
		}
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			selectquery="select account.acc_bal,transaction.acc_num from account,transaction where account.cus_id=? and transaction.cus_id=?";
			PreparedStatement stmt = con.prepareStatement(selectquery);
			stmt.setInt(1,cusId);
			stmt.setInt(2,cusId);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				accBal=rs.getInt(1);
				accNum=rs.getLong(2);
			}
			if((accBal-amount)>1000) {
			insert(type);
			update(type);
				
			}else {
				System.out.println("Transaction failed. your account  balance is less than 1000");
			}
		}catch(Exception e) {System.out.println(e);}
	}

	
	
	
	
	@Override
	public void insert(String type) {
		try {
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			insertquery="insert into transaction(acc_num,cus_id,type,amount,mode) value(?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(insertquery);
			stmt.setLong(1,accNum);
			stmt.setInt(2,cusId);
			stmt.setString(3,type);
			stmt.setInt(4,amount);
			stmt.setString(5,modeType);
			stmt.executeUpdate();
			
		}catch(Exception e) {
			System.out.println(e);
		}
	}

	
	
	
	@Override
	public void update(String type) {
		try {
		SqlConnection c = new SqlConnection();
		Connection con = c.getConnection();
		selecttwoquery="select acc_bal from account where cus_id=?";
		PreparedStatement stmt = con.prepareStatement(selecttwoquery);
		stmt.setInt(1,cusId);
		java.sql.ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			accBal=rs.getInt(1);
		}
		if(type.equals("Deposit")) {
			curBal=accBal+amount;
		}else {
			curBal=accBal-amount;
		}
		
		updateonequery="update account set acc_bal=? where cus_id=?";
		PreparedStatement prestmt = con.prepareStatement(updateonequery);
		prestmt.setInt(1,curBal);
		prestmt.setInt(2,cusId);
		prestmt.executeUpdate();
		System.out.println("Transaction success");
		}catch(Exception e) {System.out.println(e);}
	}

	
	
	
	@Override
	public void insert() {
		// TODO Auto-generated method stub
		
	}

	
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	

}
