package com.attinadsoftware.training.project1;

public interface Insertion {
     public void insert();
     public void insert(String name);
}
