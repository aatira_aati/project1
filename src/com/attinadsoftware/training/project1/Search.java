package com.attinadsoftware.training.project1;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class Search {
	String joinonequery,jointwoquery,jointhreequery;
	SqlConnection c = new SqlConnection();
	Connection con= null;
	public void doSearch(int cusId) {
		try {
		 con = c.getConnection();
		  joinonequery="select customer.Name, customer.FathersName, customer.Email,account.acc_bal from customer inner join account on account.cus_id=customer.cus_id where customer.cus_id=?";
		  PreparedStatement stmt = con.prepareStatement(joinonequery);
			stmt.setInt(1,cusId);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {		
        		System.out.print("\nName:"+rs.getString(1)+"\n Father's Name:"+rs.getString(2)+"\n Email:"+rs.getString(3)+"\n Account Balance:"+rs.getInt(4));	
			}
			rs.close();
			con.close();
		}catch(Exception e) { System.out.println(e);}
		
	}
	public void doSearch(long accNum) {
		//SqlConnection c = new SqlConnection();
		try {
		 con = c.getConnection();
		  jointwoquery="select customer.Name, customer.FathersName, customer.Email,account.acc_bal from customer inner join account on account.cus_id=customer.cus_id where account.acc_num=?";
		  PreparedStatement stmt = con.prepareStatement(jointwoquery);
			stmt.setLong(1,accNum);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {		
				System.out.print("\nName:"+rs.getString(1)+"\n Father's Name:"+rs.getString(2)+"\n Email:"+rs.getString(3)+"\n Account Balance:"+rs.getInt(4));	
			}
			rs.close();
			con.close();
		}catch(Exception e) { System.out.println(e);}
	}
	public void doSearch(String cusName) {
		
		//SqlConnection c = new SqlConnection();
		try {
		 con = c.getConnection();
		  jointhreequery="select customer.Name, customer.FathersName, customer.Email,account.acc_bal from customer inner join account on account.cus_id=customer.cus_id where customer.Name=?";
		  PreparedStatement stmt = con.prepareStatement(jointhreequery);
			stmt.setString(1,cusName);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {		
				System.out.print("\nName:"+rs.getString(1)+"\n Father's Name:"+rs.getString(2)+"\n Email:"+rs.getString(3)+"\n Account Balance:"+rs.getInt(4));	
			}
			rs.close();
			con.close();
		}catch(Exception e) { System.out.println(e);}
	}
	

}
