package com.attinadsoftware.training.project1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class NewAccount implements DataFetch,Insertion,Selection {
          
	 int cusId,amount,mode;
	 long accNum;
	 
     String accType, tranType="Deposit", tranMode="Cash";
	NewAccount(int cusId){
		this.cusId=cusId;
		this.getData();
		//this.insert();
		//this.select();
	}

	@Override
	public void getData() {
		Scanner scn = new Scanner(System.in);
		System.out.println("\nEnter the type of account you need to create- Savings or Recurring");
		 accType = scn.next();
		System.out.println("Enter the amount of initial deposit");
		amount = scn.nextInt();
		scn.close();
		insert();
		
	}

	@Override
	public void insert() {
		if(amount>=1000) {
			
			try {
					SqlConnection c = new SqlConnection();
					Connection con = c.getConnection();

					String insertquery = "insert into account(cus_id,type,acc_bal) values(?,?,?)";
					PreparedStatement prestmt = con.prepareStatement(insertquery);
					prestmt.setInt(1, this.cusId);
					prestmt.setString(2, accType);
					prestmt.setInt(3, amount);
					prestmt.executeUpdate();
					select();
					String insertquerytran = "insert into transaction(acc_num,cus_id,type,amount,mode) values(?,?,?,?,?)";
					PreparedStatement stmt = con.prepareStatement(insertquerytran);
					stmt.setLong(1,accNum);
					stmt.setInt(2,this.cusId);
					stmt.setString(3,tranType);
					stmt.setInt(4,amount);
					stmt.setString(5,tranMode);
					stmt.executeUpdate();
				    
					
					
					con.close();
				} catch (Exception e) {
					System.out.println(e);
				}
		} else {
				System.out.println("Please Deposit amount greater than or equal to 1000 ");
			}

		
		
	}

	@Override
	public void select() {
		try {
			System.out.print("Your Account number is:");
			SqlConnection c = new SqlConnection();
			Connection con = c.getConnection();
			String selectquery = "select acc_num from account where account.cus_id=?";
			PreparedStatement stmt = con.prepareStatement(selectquery);
			stmt.setInt(1,this.cusId);
			java.sql.ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				accNum=rs.getInt(1);
				System.out.print(rs.getInt(1));
			}
			
			rs.close();
			con.close();
		}catch(Exception e) {
			System.out.println(e);
		}
		
	}

	@Override
	public void insert(String name) {
		// TODO Auto-generated method stub
		
	}
	
	
}
