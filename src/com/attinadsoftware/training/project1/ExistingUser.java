package com.attinadsoftware.training.project1;

import java.util.Scanner;

public class ExistingUser implements DataFetch{
	int wish,s,cusId;
	long accNum;
	String cusName,a;
	@Override
	public void getData() {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		//do {
		System.out.println("What you wish to do?\n 1.View/Search an account\n 2.Transaction\n 3.Report");
		wish = scn.nextInt();
		doProcess();
		//System.out.println("Do you want to continue? y/n");
		// a = scn.next();
		//if(a.startsWith("n")) {break;}
		//}while(true);
		scn.close();
	}
	
     public void doProcess() {
    	 if(wish==1) {
    		 System.out.println("\n1. Search by customerid\n 2. Search by account number\n 3. Search by customer name");
    		 Search search = new Search();
    		 Scanner scn = new Scanner(System.in);
    		 s=scn.nextInt();
    		 if(s==1) {
    			 System.out.println("\nEnter the customer id");
    			 cusId=scn.nextInt();
    			search.doSearch(cusId); 
    		 }
    		 else if(s==2) {
    			 System.out.println("\nEnter the account number");
    			 accNum=scn.nextLong();
    			 search.doSearch(accNum);
    		 }
    		 else if(s==3){
    			 System.out.println("\nEnter the customer name");
    			 cusName=scn.next();
    			 search.doSearch(cusName);
    		 }
    		 scn.close();
    	 }
    	 else if(wish==2) {
    	 TransactionProcess transactionProcess = new TransactionProcess();
    	 transactionProcess.getData();
    	 }
    	 else if(wish==3){
    		 Report report = new Report();
    		 report.accountReport();
    	 }
     } 
}
