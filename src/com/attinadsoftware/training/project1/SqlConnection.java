package com.attinadsoftware.training.project1;

import java.sql.*;

public class SqlConnection {

	Connection con = null;

	public Connection getConnection() {
		// Connection con=null;
		String myDriver = "com.mysql.jdbc.Driver";
		String myUrl = "jdbc:mysql://localhost:3306/bank";

		try {
			Class.forName(myDriver);
			con = DriverManager.getConnection(myUrl, "root", "root");
		} catch (Exception e) {

			e.printStackTrace();
		}

		return con;

	}
}